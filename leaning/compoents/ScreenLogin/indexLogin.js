import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FooterLogin from './FooterLogin';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../shareImage/Login';
export default class IndexLoginDemo extends Component {
  render() {
    const Stack =createStackNavigator();
    return (
      <View style={styles.container}>
        <View style={{flex: 11, flexDirection: 'column'}}>
          <ScrollView>
            <Image
              style={styles.logoStem}
              source={require('../../assets/images/logo_stemup2.png')}
            />
            <Text
              style={{
                alignSelf: 'center',
                fontSize: 23,
                marginTop: 30,
                color: 'white',
              }}>
              Phụ Huynh
            </Text>
            <View style={styles.down}>
              <View style={styles.TextInputContainer}>
                <TextInput
                  style={styles.TextInput}
                  placeholder={'Email hoặc số điện thoại'}
                  placeholderTextColor="white"
                  textContentType="emailAddress"
                  keyboardType="email-address"
                />
              </View>
              <View style={styles.TextInputContainer}>
                <TextInput
                  style={styles.TextInput}
                  placeholder={'Mật Khẩu'}
                  placeholderTextColor="white"
                  secureTextEntry={true}
                />
                <TouchableOpacity>
                  <Icon name="eye" style={styles.iconStyle}></Icon>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.btnLogin}>
              <TouchableOpacity>
                <Text
                  style={{
                    fontSize: 20,
                    marginTop: 10,
                    alignSelf: 'center',
                    color: 'white',
                  }}
                  onPress={() => {
                    alert('Đăng nhập không thành công');
                  }}>
                  Đăng Nhập
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10,
              }}>
              <TouchableOpacity>
                <Text
                  style={styles.DangKiQuenMatKhau}
                  onPress={() => {
                    alert('Bạn muốn đăng ký?');
                  }}>
                  Đăng Kí
                </Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text
                  style={styles.DangKiQuenMatKhau}
                  onPress={() => {
                    alert('Bạn muốn lấy lại mật khẩu?');
                  }}>
                  Quên Mật Khẩu
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
        <FooterLogin />
{/*         
        <NavigationContainer>
          <Stack.Screen name="Login" component={Login}></Stack.Screen>
        </NavigationContainer> */}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E86657',
  },
  logoStem: {
    width: 300,
    height: 100,
    marginTop: 50,
  },
  footer: {
    width: 500,
    height: 200,
  },
  Login: {
    borderBottomWidth: 1,
    borderColor: '#D9D5DC',
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputStyle: {
    color: 'white',
    paddingRight: 16,
    fontSize: 16,
    alignSelf: 'stretch',
    flex: 1,
    lineHeight: 16,
    paddingTop: 14,
    paddingBottom: 8,
  },
  iconStyle: {
    color: '#616161',
    fontSize: 24,
    paddingRight: 8,
  },
  TextInputContainer: {
    borderBottomWidth: 1,
    borderColor: '#D9D5DC',
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 30,
  },
  TextInput: {
    color: 'white',
    fontSize: 16,
    flex: 1,
    backgroundColor: 'rgba(255,255,255,0)',
  },
  btnLogin: {
    height: 55,
    marginTop: 30,
    borderColor: 'white',
    borderRadius: 50,
    borderWidth: 1,
  },
  DangKiQuenMatKhau: {
    fontSize: 16,
    marginTop: 10,
    alignSelf: 'center',
    color: 'white',
    borderBottomWidth: 1,
    borderColor: 'white',
  },
  iconStyle: {
    color: '#616161',
    fontSize: 24,
    paddingRight: 8,
  },
});
