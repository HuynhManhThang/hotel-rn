import React, { Component } from 'react'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { NavigationContainer } from '@react-navigation/native/lib/typescript/src';
import indexSreenTow from './indexTow';


const Tab  =createMaterialBottomTabNavigator();

export default class TabNavigator extends Component {
    render() {
        return (
            <NavigationContainer>
            <Tab.Navigator>
              <Tab.Screen name="Nhiemvu" component={indexSreenTow} options={{ headerShown: false }}/>
              <Tab.Screen name="Settings" component={SettingsScreen} />
            </Tab.Navigator>
          </NavigationContainer>
        )
    }
}
