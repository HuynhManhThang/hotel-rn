import React, { Component } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native';
import LoginStack from './login';


const Stack= createStackNavigator();

export default class IndexTabNavigation extends Component {

    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen name="login" component={LoginStack}></Stack.Screen>
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
}
