import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import HeaderScreenOne from "./header";
import FooterALL from "../Footer";

export default class IndexSreenOne extends Component {
  render() {
    return (
      <View style={styles.container}>
      <HeaderScreenOne
        style={styles.HeaderScreenOne}
      ></HeaderScreenOne>      
      <FooterALL style={styles.FooterALL}></FooterALL>
    </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  HeaderScreenOne: {
    height: 44,
    width: 'auto'
    
  },
 
  FooterALL: {
    height: 49,
    width: 'auto',   
    marginLeft: 2
  }
});