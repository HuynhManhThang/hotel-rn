import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { log } from 'react-native-reanimated';

export default class Login extends Component {
  render() {
    return (
      <View style={styles.container}>
       
        <View style={styles.down}>
        <Image
            style={styles.logo}
            source={require('../../assets/images/logo_stemup2.png')}
          />
          <Text style={styles.textPhuHuynh}>
            Phụ Huynh
          </Text>
            <TextInput style={styles.TextInput} placeholder='Email hoặc số điện thoại' placeholderTextColor='white' textContentType='emailAddress' keyboardType='email-address'/>
            <TextInput style={styles.TextInput1} placeholder='Mật khẩu' placeholderTextColor='white' secureTextEntry={true}/>
            <View style={styles.btnLogin}>
              <TouchableOpacity >
                <Text style={{ fontSize: 20, marginTop: 10, alignSelf: 'center', color: 'white', }}>
                  Đăng Nhập
                </Text>
              </TouchableOpacity>
            </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E86657',
  },
 
  down: {
    flex: 8,
    flexDirection: 'column',
  },
  logo: {
    width: 300,
    height: 100,
    marginTop: 50,
  },
  textPhuHuynh:{
    alignSelf: 'center',
    fontSize: 23,
    marginTop: 30,
    color: 'white',
  },
  TextInput:{
      width:300,
      height:45,
      borderBottomWidth:1,
      borderColor:'white',
      marginTop:100
  },
  TextInput1:{
    width:300,
    height:45,
    borderBottomWidth:1,
    borderColor:'white',
    marginTop:25
},
loginButton:{
    width:300,
    height:50,
    borderRadius:50,
    borderWidth:1,
    marginTop:25,
    justifyContent:"center",
    alignItems:'center',
    borderColor:'white'
  
},
btnLogin: {
    height: 55,
    marginTop: 50,
    borderColor: '#D9D5DC',
    borderRadius: 30,
    backgroundColor: 'rgba(255,255,255,0)',
    borderStartWidth: 1,
    borderEndWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
});
