import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  ScrollViewComponent,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconf from "react-native-vector-icons/FontAwesome";

export default class indexSreenTow extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{flex: 0.1, backgroundColor: '#E86657'}}>
          <Image
            style={styles.imageLogo}
            source={{
              uri:
                'https://www.americandream.edu.vn/statics/uploads/2019/02/STEM.png',
            }}
          />
        </View>
        <ScrollView style={{flex: 1}}>
        <View style={styles.ItemHome}>
            <View style={styles.flexColumn}>
              <View style={styles.flexRow}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={styles.avatarUser}
                    source={{
                      uri:
                        'https://lh3.googleusercontent.com/proxy/7nQuSf7XVpaL7GCbjg4yHduZHofpIWjRgIATbYSWGCpNBe-u8qnMUkyrebNIPhc4pZkDKHgBOpl2QeQTDSOhWTJiJDnC3QPZ2EPrQMRsv8apuoG2WP2YpXJyniRsWmDz014Ku743PfIQeoLaccJ0rE-we4dj',
                    }}
                  />
                  <View style={{flexDirection: 'column'}}>
                    <Text
                      style={{marginLeft: 20, fontSize: 18, color: '#949494'}}>
                      Thảo Mai
                    </Text>
                    <Text
                      style={{marginLeft: 20, fontSize: 18, color: '#949494'}}>
                      Lớp 8
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Icon name="ios-star" style={styles.icon}></Icon>
                  <Text style={{marginRight: 20, fontSize: 24, color: 'red'}}>
                    550
                  </Text>
                </View>
              </View>
              <View style={styles.flexRow}>
                <TouchableOpacity style={styles.ItemSkillLeft}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://i.pinimg.com/280x280_RS/37/2d/ab/372dabf6e8d3bb1292f5ed79af3c78db.jpg',
                      }}
                    />
                    <Text style={styles.Text}>Kĩ Năng Sống</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.ItemSkillRight}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcThjtmMUOW6JAW8S6VlI--X565vNOx4FKUxYwcILUIZWHgfRGfO&usqp=CAU',
                      }}
                    />
                    <Text style={styles.Text}>Học Cùng Con</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.flexRow}>
                <TouchableOpacity style={styles.ItemSkillLeft}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://vinuni.edu.vn/wp-content/uploads/2019/03/dd3703c08a47b6b6ad238ebd5ea5e303-stems-literacy-e1551933675894.jpg',
                      }}
                    />
                    <Text style={styles.Text}>STEM</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.ItemHome}>
            <View style={styles.flexColumn}>
              <View style={styles.flexRow}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={styles.avatarUser}
                    source={{
                      uri:
                        'https://i.pinimg.com/236x/76/80/76/7680768d2115009e96ad70bd57146e74.jpg',
                    }}
                  />
                  <View style={{flexDirection: 'column'}}>
                    <Text
                      style={{marginLeft: 20, fontSize: 18, color: '#949499'}}>
                      Thích thì Học
                    </Text>
                    <Text
                      style={{marginLeft: 20, fontSize: 18, color: '#949499'}}>
                      Lớp 8
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Icon name="ios-star" style={styles.icon}></Icon>
                  <Text style={{marginRight: 20, fontSize: 24, color: 'red'}}>
                    4521
                  </Text>
                </View>
              </View>
              <View style={styles.flexRow}>
                <TouchableOpacity style={styles.ItemSkillLeft}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://i.pinimg.com/280x280_RS/37/2d/ab/372dabf6e8d3bb1292f5ed79af3c78db.jpg',
                      }}
                    />
                    <Text style={styles.Text}>Kĩ Năng Sống</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.ItemSkillRight}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcThjtmMUOW6JAW8S6VlI--X565vNOx4FKUxYwcILUIZWHgfRGfO&usqp=CAU',
                      }}
                    />
                    <Text style={styles.Text}>Học Cùng Con</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.flexRow}>
                <TouchableOpacity style={styles.ItemSkillLeft}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://vinuni.edu.vn/wp-content/uploads/2019/03/dd3703c08a47b6b6ad238ebd5ea5e303-stems-literacy-e1551933675894.jpg',
                      }}
                    />
                    <Text style={styles.Text}>STEM</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.ItemHome}>
            <View style={styles.flexColumn}>
              <View style={styles.flexRow}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={styles.avatarUser}
                    source={{
                      uri:
                        'https://lh3.googleusercontent.com/proxy/7nQuSf7XVpaL7GCbjg4yHduZHofpIWjRgIATbYSWGCpNBe-u8qnMUkyrebNIPhc4pZkDKHgBOpl2QeQTDSOhWTJiJDnC3QPZ2EPrQMRsv8apuoG2WP2YpXJyniRsWmDz014Ku743PfIQeoLaccJ0rE-we4dj',
                    }}
                  />
                  <View style={{flexDirection: 'column'}}>
                    <Text
                      style={{marginLeft: 20, fontSize: 18, color: '#949494'}}>
                      Thảo Mai
                    </Text>
                    <Text
                      style={{marginLeft: 20, fontSize: 18, color: '#949494'}}>
                      Lớp 8
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Icon name="ios-star" style={styles.icon}></Icon>
                  <Text style={{marginRight: 20, fontSize: 24, color: 'red'}}>
                    550
                  </Text>
                </View>
              </View>
              <View style={styles.flexRow}>
                <TouchableOpacity style={styles.ItemSkillLeft}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://i.pinimg.com/280x280_RS/37/2d/ab/372dabf6e8d3bb1292f5ed79af3c78db.jpg',
                      }}
                    />
                    <Text style={styles.Text}>Kĩ Năng Sống</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.ItemSkillRight}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcThjtmMUOW6JAW8S6VlI--X565vNOx4FKUxYwcILUIZWHgfRGfO&usqp=CAU',
                      }}
                    />
                    <Text style={styles.Text}>Học Cùng Con</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.flexRow}>
                <TouchableOpacity style={styles.ItemSkillLeft}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://vinuni.edu.vn/wp-content/uploads/2019/03/dd3703c08a47b6b6ad238ebd5ea5e303-stems-literacy-e1551933675894.jpg',
                      }}
                    />
                    <Text style={styles.Text}>STEM</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
   
          <View style={styles.ItemHome}>
            <View style={styles.flexColumn}>
              <View style={styles.flexRow}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={styles.avatarUser}
                    source={{
                      uri:
                        'https://lh3.googleusercontent.com/proxy/7nQuSf7XVpaL7GCbjg4yHduZHofpIWjRgIATbYSWGCpNBe-u8qnMUkyrebNIPhc4pZkDKHgBOpl2QeQTDSOhWTJiJDnC3QPZ2EPrQMRsv8apuoG2WP2YpXJyniRsWmDz014Ku743PfIQeoLaccJ0rE-we4dj',
                    }}
                  />
                  <View style={{flexDirection: 'column'}}>
                    <Text
                      style={{marginLeft: 20, fontSize: 18, color: '#949494'}}>
                      Thảo Mai
                    </Text>
                    <Text
                      style={{marginLeft: 20, fontSize: 18, color: '#949494'}}>
                      Lớp 8
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Icon name="ios-star" style={styles.icon}></Icon>
                  <Text style={{marginRight: 20, fontSize: 24, color: 'red'}}>
                    550
                  </Text>
                </View>
              </View>
              <View style={styles.flexRow}>
                <TouchableOpacity style={styles.ItemSkillLeft}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://i.pinimg.com/280x280_RS/37/2d/ab/372dabf6e8d3bb1292f5ed79af3c78db.jpg',
                      }}
                    />
                    <Text style={styles.Text}>Kĩ Năng Sống</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.ItemSkillRight}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcThjtmMUOW6JAW8S6VlI--X565vNOx4FKUxYwcILUIZWHgfRGfO&usqp=CAU',
                      }}
                    />
                    <Text style={styles.Text}>Học Cùng Con</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.flexRow}>
                <TouchableOpacity style={styles.ItemSkillLeft}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://vinuni.edu.vn/wp-content/uploads/2019/03/dd3703c08a47b6b6ad238ebd5ea5e303-stems-literacy-e1551933675894.jpg',
                      }}
                    />
                    <Text style={styles.Text}>STEM</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.ItemHome}>
            <View style={styles.flexColumn}>
              <View style={styles.flexRow}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={styles.avatarUser}
                    source={{
                      uri:
                        'https://i.pinimg.com/236x/76/80/76/7680768d2115009e96ad70bd57146e74.jpg',
                    }}
                  />
                  <View style={{flexDirection: 'column'}}>
                    <Text
                      style={{marginLeft: 20, fontSize: 18, color: '#949499'}}>
                      Thích thì Học
                    </Text>
                    <Text
                      style={{marginLeft: 20, fontSize: 18, color: '#949499'}}>
                      Lớp 8
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Icon name="ios-star" style={styles.icon}></Icon>
                  <Text style={{marginRight: 20, fontSize: 24, color: 'red'}}>
                    4521
                  </Text>
                </View>
              </View>
              <View style={styles.flexRow}>
                <TouchableOpacity style={styles.ItemSkillLeft}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://i.pinimg.com/280x280_RS/37/2d/ab/372dabf6e8d3bb1292f5ed79af3c78db.jpg',
                      }}
                    />
                    <Text style={styles.Text}>Kĩ Năng Sống</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.ItemSkillRight}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcThjtmMUOW6JAW8S6VlI--X565vNOx4FKUxYwcILUIZWHgfRGfO&usqp=CAU',
                      }}
                    />
                    <Text style={styles.Text}>Học Cùng Con</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.flexRow}>
                <TouchableOpacity style={styles.ItemSkillLeft}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://vinuni.edu.vn/wp-content/uploads/2019/03/dd3703c08a47b6b6ad238ebd5ea5e303-stems-literacy-e1551933675894.jpg',
                      }}
                    />
                    <Text style={styles.Text}>STEM</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.ItemHome}>
            <View style={styles.flexColumn}>
              <View style={styles.flexRow}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={styles.avatarUser}
                    source={{
                      uri:
                        'https://lh3.googleusercontent.com/proxy/7nQuSf7XVpaL7GCbjg4yHduZHofpIWjRgIATbYSWGCpNBe-u8qnMUkyrebNIPhc4pZkDKHgBOpl2QeQTDSOhWTJiJDnC3QPZ2EPrQMRsv8apuoG2WP2YpXJyniRsWmDz014Ku743PfIQeoLaccJ0rE-we4dj',
                    }}
                  />
                  <View style={{flexDirection: 'column'}}>
                    <Text
                      style={{marginLeft: 20, fontSize: 18, color: '#949494'}}>
                      Thảo Mai
                    </Text>
                    <Text
                      style={{marginLeft: 20, fontSize: 18, color: '#949494'}}>
                      Lớp 8
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Icon name="ios-star" style={styles.icon}></Icon>
                  <Text style={{marginRight: 20, fontSize: 24, color: 'red'}}>
                    550
                  </Text>
                </View>
              </View>
              <View style={styles.flexRow}>
                <TouchableOpacity style={styles.ItemSkillLeft}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://i.pinimg.com/280x280_RS/37/2d/ab/372dabf6e8d3bb1292f5ed79af3c78db.jpg',
                      }}
                    />
                    <Text style={styles.Text}>Kĩ Năng Sống</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.ItemSkillRight}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcThjtmMUOW6JAW8S6VlI--X565vNOx4FKUxYwcILUIZWHgfRGfO&usqp=CAU',
                      }}
                    />
                    <Text style={styles.Text}>Học Cùng Con</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.flexRow}>
                <TouchableOpacity style={styles.ItemSkillLeft}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={styles.avatarSubject}
                      source={{
                        uri:
                          'https://vinuni.edu.vn/wp-content/uploads/2019/03/dd3703c08a47b6b6ad238ebd5ea5e303-stems-literacy-e1551933675894.jpg',
                      }}
                    />
                    <Text style={styles.Text}>STEM</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
       </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ItemHome: {
    width: '100%',
    height: 340,
    marginTop: 10,
    backgroundColor: 'white',
    shadowColor: 'rgba(0,0,0,1)',
    shadowOffset: {
      width: 3,
      height: 3,
    },
    elevation: 10,
    shadowOpacity: 0.2,
    shadowRadius: 11,
    overflow: 'scroll',
  },
  flexcolumn: {
    flexDirection: 'column',
  },
  flexRow: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  avatarUser: {
    width: 80,
    height: 80,
    borderRadius: 50,
    marginLeft: 5,
  },
  avatarSubject: {
    width: 70,
    height: 70,
    borderRadius: 50,
    marginTop: 5,
    marginLeft: 5,
  },
  icon: {
    color: 'rgba(248,231,28,1)',
    fontSize: 30,
    width: 35,
  },
  ItemSkillLeft: {
    backgroundColor: '#92DD00',
    width: '45%',
    height: 84,
    borderBottomLeftRadius: 30,
    borderTopRightRadius: 30,
    borderRadius: 5,
  },
  ItemSkillRight: {
    backgroundColor: '#FA5340',
    width: '45%',
    height: 84,
    borderBottomLeftRadius: 30,
    borderTopRightRadius: 30,
    borderRadius: 5,
  },
  Text: {
    marginTop: 20,
    marginLeft: 10,
    fontSize: 16,
    color: 'white',
    width: 85,
  },
  imageLogo: {
    width: '40%',
    height: 50,
    marginTop: 10,
    alignSelf: 'center',
  },

  icon1: {
    color: '#A4A8AD',
    fontSize: 28,
    opacity: 0.8,
  },
  btnNhiemVu: {
    flex: 0.27,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    backgroundColor: 'transparent',
    fontSize: 24,
    opacity: 0.8,
  },
  NhiemVu: {
    fontSize: 10,
    backgroundColor: 'transparent',
    paddingTop: 4,
  },
  btnThongBao: {
    flex: 0.23,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon1: {
    backgroundColor: 'transparent',
    color: '#616161',
    fontSize: 24,
    opacity: 0.8,
  },
  ThongBao: {
    fontSize: 10,
    color: '#9E9E9E',
    backgroundColor: 'transparent',
    paddingTop: 4,
  },
  btnCauHinh: {
    flex: 0.21,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon2: {
    backgroundColor: 'transparent',
    color: '#616161',
    fontSize: 24,
    opacity: 0.8,
  },
  CauHinh: {
    fontSize: 10,
    color: '#9E9E9E',
    backgroundColor: 'transparent',
    paddingTop: 4,
  },
});
