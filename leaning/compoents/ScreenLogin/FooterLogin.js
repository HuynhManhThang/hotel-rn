import React, { Component } from 'react'
import { StyleSheet, View, ImageBackground, Text, Dimensions } from 'react-native'

export default class FooterLogin extends Component {
    render() {
        return (
           
        <View  style={{flex:1.7,bottom:0}}>
        <ImageBackground
          source={require("../../assets/images/cloudFooter.png")}
          style={styles.image}>
          <Text style={styles.title}>Dự án tích hợp Hệ Tri thức Việt số hóa ithuc.vn</Text>
        </ImageBackground>
        </View>
        )
    }
}
const styles=StyleSheet.create({
    image: {
        width:Dimensions.get('window').width*1.1,
        height:100,
        justifyContent: 'center',
        alignItems:'center',
        position:'relative',
        // bottom:0
      },
      title: {
        color: "#E86657",
        marginTop:30
      }
})
