import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import IoniconsIcon from "react-native-vector-icons/Ionicons";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import HomeEdu from "./Home"
function HeaderScreenOne(props) {
  return (
    <View>
      <View style={[styles.container, props.style]}>
        <View style={styles.iconRow}>
          <TouchableOpacity>
            <IoniconsIcon
              name="md-checkmark-circle-outline"
              style={styles.icon} />
          </TouchableOpacity>
          <TouchableOpacity>
            <IoniconsIcon name="ios-search" style={styles.icon3} />
          </TouchableOpacity>
          <TouchableOpacity>
            <FontAwesomeIcon
              name="user-circle"
              style={styles.icon2} />
          </TouchableOpacity>
        </View>

      </View>

      <View style={styles.homeEdu}>
        <HomeEdu />
      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    backgroundColor: "rgba(143,143,203,1)",
    paddingRight: 8,
    paddingLeft: 8
    
  },
  homeEdu:{
    
    marginLeft:20,
    marginRight:20,

  },
  icon: {
    color: "rgba(246,239,239,1)",
    fontSize: 40
  },
  icon3: {
    color: "rgba(232,227,227,1)",
    fontSize: 40,
    marginLeft: 247
  },
  icon2: {
    color: "rgba(248,242,242,1)",
    fontSize: 36,
    opacity: 0.92,
    width: 36,
    height: 36,
    marginLeft: 30,
    marginTop: 3
  },
  iconRow: {
    height: 44,
    flexDirection: "row",
    flex: 1,
    marginRight: 8,
    marginLeft: 11
  }
});

export default HeaderScreenOne;
