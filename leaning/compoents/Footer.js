import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import IoniconsIcon from "react-native-vector-icons/Ionicons";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";

function FooterALL(props) {
  return (
    <View style={[styles.container, props.style]}>
    
      <TouchableOpacity style={styles.btnWrapper1}>
        <IoniconsIcon
          name="ios-home"
          style={[
            styles.icon,
            {
              color: props.active ? "#007AFF" : "#616161"
            }
          ]}
        ></IoniconsIcon>
        <Text
          style={[
            styles.trangChủ,
            {
              color: props.active ? "#007AFF" : "#9E9E9E"
            }
          ]}
        >
          Trang chủ
        </Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.btnWrapper2}>
        <IoniconsIcon
          name="ios-arrow-dropdown-circle"
          style={styles.icon1}
        ></IoniconsIcon>
        <Text style={styles.trắcNghiệm}>Trắc nghiệm</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.btnWrapper3}>
        <IoniconsIcon name="ios-keypad" style={styles.icon2}></IoniconsIcon>
        <Text style={styles.yeuThich}>Yêu thích</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.btnWrapper4}>
        <FontAwesomeIcon name="bell" style={styles.icon3}></FontAwesomeIcon>
        <Text style={styles.thongBao}>Thông báo</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.btnWrapper5}>
        <IoniconsIcon name="ios-book" style={styles.icon4}></IoniconsIcon>
        <Text style={styles.thưViện}>Thư viện</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row",
    width: "100%",
    
  },
  btnWrapper1: {
    flex: 0.27,
    alignItems: "center",
    justifyContent: "center"
  },
  icon: {
    backgroundColor: "transparent",
    fontSize: 24,
    opacity: 0.8
  },
  trangChủ: {
    fontSize: 10,
    backgroundColor: "transparent",
    paddingTop: 4
  },
  btnWrapper2: {
    flex: 0.23,
    alignItems: "center",
    justifyContent: "center"
  },
  icon1: {
    backgroundColor: "transparent",
    color: "#616161",
    fontSize: 24,
    opacity: 0.8
  },
  trắcNghiệm: {
    fontSize: 10,
    color: "#9E9E9E",
    backgroundColor: "transparent",
    paddingTop: 4
  },
  btnWrapper3: {
    flex: 0.21,
    alignItems: "center",
    justifyContent: "center"
  },
  icon2: {
    backgroundColor: "transparent",
    color: "#616161",
    fontSize: 24,
    opacity: 0.8
  },
  yeuThich: {
    fontSize: 10,
    color: "#9E9E9E",
    backgroundColor: "transparent",
    paddingTop: 4
  },
  btnWrapper4: {
    flex: 0.29,
    alignItems: "center",
    justifyContent: "center"
  },
  icon3: {
    backgroundColor: "transparent",
    color: "#616161",
    fontSize: 24,
    opacity: 0.8
  },
  thongBao: {
    fontSize: 10,
    color: "#9E9E9E",
    backgroundColor: "transparent",
    paddingTop: 4
  },
  btnWrapper5: {
    flex: 0.29,
    alignItems: "center",
    justifyContent: "center"
  },
  icon4: {
    backgroundColor: "transparent",
    color: "#616161",
    fontSize: 24,
    opacity: 0.8
  },
  thưViện: {
    fontSize: 10,
    color: "#9E9E9E",
    backgroundColor: "transparent",
    paddingTop: 4
  }
});

export default FooterALL;
