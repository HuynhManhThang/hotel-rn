import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, StyleSheet, ScrollView } from 'react-native';

export default class HomeEdu extends Component {
    render() {
        return (
            <View style={styles.container}>
            <ScrollView>
            
            <View style={styles.flexRow}>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://3.bp.blogspot.com/-wLxVSkn7UFE/WQ-EQXJZTZI/AAAAAAAAFs4/NpL7xT0ErYEj-MBGM6mq8RsYj-S_mawaQCLcB/s1600/Evans-Pi-1.png',
                                }} />
                            <Text style={styles.TextPri}>Toán(12)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://cdn.pixabay.com/photo/2012/04/26/18/11/nucleus-42693_960_720.png',
                                }} />
                            <Text style={styles.TextPri}>Vật Lý(18)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://lh3.googleusercontent.com/proxy/FYlo5EFWH82p8lOrbJZ3B0HJZ2p18dHsikxX8bNVdpDyr4Fb_znfJSXAW1ZSRTeJaSjSAruBWH5QhaL6nsFF2AoaOGSaJaX5uM7eQzq8hz1i_UEofbK4Hl8iaimpgR1_3JKU7GcWRYWWRlrTMXFSCKCj9L7W7HoRXp_YzCfY_sylbqQyHlBlBhZAfb-RntS_BUmlBQ',
                                }} />
                            <Text style={styles.TextPri}>Hóa Học(24)</Text>
                        </View>
                    </TouchableOpacity>

                </View>
                <View style={styles.flexRow}>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                          
                                <Image
                                    style={styles.tinyLogo}
                                    source={{
                                        uri: 'https://3.bp.blogspot.com/-wLxVSkn7UFE/WQ-EQXJZTZI/AAAAAAAAFs4/NpL7xT0ErYEj-MBGM6mq8RsYj-S_mawaQCLcB/s1600/Evans-Pi-1.png',
                                    }} />
                                    <Text style={styles.TextPri}>Địa lý(12)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://cdn.pixabay.com/photo/2012/04/26/18/11/nucleus-42693_960_720.png',
                                }} />
                            <Text style={styles.TextPri}>Tin Học(18)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://lh3.googleusercontent.com/proxy/FYlo5EFWH82p8lOrbJZ3B0HJZ2p18dHsikxX8bNVdpDyr4Fb_znfJSXAW1ZSRTeJaSjSAruBWH5QhaL6nsFF2AoaOGSaJaX5uM7eQzq8hz1i_UEofbK4Hl8iaimpgR1_3JKU7GcWRYWWRlrTMXFSCKCj9L7W7HoRXp_YzCfY_sylbqQyHlBlBhZAfb-RntS_BUmlBQ',
                                }} />
                            <Text style={styles.TextPri}>Sinh Học(24)</Text>
                        </View>
                    </TouchableOpacity>

                </View>
                <View style={styles.flexRow}>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://3.bp.blogspot.com/-wLxVSkn7UFE/WQ-EQXJZTZI/AAAAAAAAFs4/NpL7xT0ErYEj-MBGM6mq8RsYj-S_mawaQCLcB/s1600/Evans-Pi-1.png',
                                }} />
                            <Text style={styles.TextPri}>Khoa Học(12)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://cdn.pixabay.com/photo/2012/04/26/18/11/nucleus-42693_960_720.png',
                                }} />
                            <Text style={styles.TextPri}>Lịch sử(18)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://lh3.googleusercontent.com/proxy/FYlo5EFWH82p8lOrbJZ3B0HJZ2p18dHsikxX8bNVdpDyr4Fb_znfJSXAW1ZSRTeJaSjSAruBWH5QhaL6nsFF2AoaOGSaJaX5uM7eQzq8hz1i_UEofbK4Hl8iaimpgR1_3JKU7GcWRYWWRlrTMXFSCKCj9L7W7HoRXp_YzCfY_sylbqQyHlBlBhZAfb-RntS_BUmlBQ',
                                }} />
                            <Text style={styles.TextPri}>Công Nghệ(24)</Text>
                        </View>
                    </TouchableOpacity>

                </View>
                <View style={styles.flexRow}>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://3.bp.blogspot.com/-wLxVSkn7UFE/WQ-EQXJZTZI/AAAAAAAAFs4/NpL7xT0ErYEj-MBGM6mq8RsYj-S_mawaQCLcB/s1600/Evans-Pi-1.png',
                                }} />
                            <Text style={styles.TextPri}>Tiếng Anh(12)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://cdn.pixabay.com/photo/2012/04/26/18/11/nucleus-42693_960_720.png',
                                }} />
                            <Text style={styles.TextPri}>Thiên Văn-Vũ Trụ(18)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://lh3.googleusercontent.com/proxy/FYlo5EFWH82p8lOrbJZ3B0HJZ2p18dHsikxX8bNVdpDyr4Fb_znfJSXAW1ZSRTeJaSjSAruBWH5QhaL6nsFF2AoaOGSaJaX5uM7eQzq8hz1i_UEofbK4Hl8iaimpgR1_3JKU7GcWRYWWRlrTMXFSCKCj9L7W7HoRXp_YzCfY_sylbqQyHlBlBhZAfb-RntS_BUmlBQ',
                                }} />
                            <Text style={styles.TextPri}>RoBot(24)</Text>
                        </View>
                    </TouchableOpacity>

                </View>
                <View style={styles.flexRow}>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://3.bp.blogspot.com/-wLxVSkn7UFE/WQ-EQXJZTZI/AAAAAAAAFs4/NpL7xT0ErYEj-MBGM6mq8RsYj-S_mawaQCLcB/s1600/Evans-Pi-1.png',
                                }} />
                            <Text style={styles.TextPri}>Môi Trường(12)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://cdn.pixabay.com/photo/2012/04/26/18/11/nucleus-42693_960_720.png',
                                }} />
                            <Text style={styles.TextPri}>Sức Khỏe(18)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://lh3.googleusercontent.com/proxy/FYlo5EFWH82p8lOrbJZ3B0HJZ2p18dHsikxX8bNVdpDyr4Fb_znfJSXAW1ZSRTeJaSjSAruBWH5QhaL6nsFF2AoaOGSaJaX5uM7eQzq8hz1i_UEofbK4Hl8iaimpgR1_3JKU7GcWRYWWRlrTMXFSCKCj9L7W7HoRXp_YzCfY_sylbqQyHlBlBhZAfb-RntS_BUmlBQ',
                                }} />
                            <Text style={styles.TextPri}>Văn Học(24)</Text>
                        </View>
                    </TouchableOpacity>

                </View>
              
                <View style={styles.flexRow}>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://3.bp.blogspot.com/-wLxVSkn7UFE/WQ-EQXJZTZI/AAAAAAAAFs4/NpL7xT0ErYEj-MBGM6mq8RsYj-S_mawaQCLcB/s1600/Evans-Pi-1.png',
                                }} />
                            <Text style={styles.TextPri}>Toán(12)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://cdn.pixabay.com/photo/2012/04/26/18/11/nucleus-42693_960_720.png',
                                }} />
                            <Text style={styles.TextPri}>Vật Lý(18)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://lh3.googleusercontent.com/proxy/FYlo5EFWH82p8lOrbJZ3B0HJZ2p18dHsikxX8bNVdpDyr4Fb_znfJSXAW1ZSRTeJaSjSAruBWH5QhaL6nsFF2AoaOGSaJaX5uM7eQzq8hz1i_UEofbK4Hl8iaimpgR1_3JKU7GcWRYWWRlrTMXFSCKCj9L7W7HoRXp_YzCfY_sylbqQyHlBlBhZAfb-RntS_BUmlBQ',
                                }} />
                            <Text style={styles.TextPri}>Hóa Học(24)</Text>
                        </View>
                    </TouchableOpacity>

                </View>
                <View style={styles.flexRow}>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                          
                                <Image
                                    style={styles.tinyLogo}
                                    source={{
                                        uri: 'https://3.bp.blogspot.com/-wLxVSkn7UFE/WQ-EQXJZTZI/AAAAAAAAFs4/NpL7xT0ErYEj-MBGM6mq8RsYj-S_mawaQCLcB/s1600/Evans-Pi-1.png',
                                    }} />
                                    <Text style={styles.TextPri}>Địa lý(12)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://cdn.pixabay.com/photo/2012/04/26/18/11/nucleus-42693_960_720.png',
                                }} />
                            <Text style={styles.TextPri}>Tin Học(18)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://lh3.googleusercontent.com/proxy/FYlo5EFWH82p8lOrbJZ3B0HJZ2p18dHsikxX8bNVdpDyr4Fb_znfJSXAW1ZSRTeJaSjSAruBWH5QhaL6nsFF2AoaOGSaJaX5uM7eQzq8hz1i_UEofbK4Hl8iaimpgR1_3JKU7GcWRYWWRlrTMXFSCKCj9L7W7HoRXp_YzCfY_sylbqQyHlBlBhZAfb-RntS_BUmlBQ',
                                }} />
                            <Text style={styles.TextPri}>Sinh Học(24)</Text>
                        </View>
                    </TouchableOpacity>

                </View>
                <View style={styles.flexRow}>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://3.bp.blogspot.com/-wLxVSkn7UFE/WQ-EQXJZTZI/AAAAAAAAFs4/NpL7xT0ErYEj-MBGM6mq8RsYj-S_mawaQCLcB/s1600/Evans-Pi-1.png',
                                }} />
                            <Text style={styles.TextPri}>Khoa Học(12)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://cdn.pixabay.com/photo/2012/04/26/18/11/nucleus-42693_960_720.png',
                                }} />
                            <Text style={styles.TextPri}>Lịch sử(18)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://lh3.googleusercontent.com/proxy/FYlo5EFWH82p8lOrbJZ3B0HJZ2p18dHsikxX8bNVdpDyr4Fb_znfJSXAW1ZSRTeJaSjSAruBWH5QhaL6nsFF2AoaOGSaJaX5uM7eQzq8hz1i_UEofbK4Hl8iaimpgR1_3JKU7GcWRYWWRlrTMXFSCKCj9L7W7HoRXp_YzCfY_sylbqQyHlBlBhZAfb-RntS_BUmlBQ',
                                }} />
                            <Text style={styles.TextPri}>Công Nghệ(24)</Text>
                        </View>
                    </TouchableOpacity>

                </View>
                <View style={styles.flexRow}>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://3.bp.blogspot.com/-wLxVSkn7UFE/WQ-EQXJZTZI/AAAAAAAAFs4/NpL7xT0ErYEj-MBGM6mq8RsYj-S_mawaQCLcB/s1600/Evans-Pi-1.png',
                                }} />
                            <Text style={styles.TextPri}>Tiếng Anh(12)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://cdn.pixabay.com/photo/2012/04/26/18/11/nucleus-42693_960_720.png',
                                }} />
                            <Text style={styles.TextPri}>Thiên Văn-Vũ Trụ(18)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://lh3.googleusercontent.com/proxy/FYlo5EFWH82p8lOrbJZ3B0HJZ2p18dHsikxX8bNVdpDyr4Fb_znfJSXAW1ZSRTeJaSjSAruBWH5QhaL6nsFF2AoaOGSaJaX5uM7eQzq8hz1i_UEofbK4Hl8iaimpgR1_3JKU7GcWRYWWRlrTMXFSCKCj9L7W7HoRXp_YzCfY_sylbqQyHlBlBhZAfb-RntS_BUmlBQ',
                                }} />
                            <Text style={styles.TextPri}>RoBot(24)</Text>
                        </View>
                    </TouchableOpacity>

                </View>
                <View style={styles.flexRow}>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://3.bp.blogspot.com/-wLxVSkn7UFE/WQ-EQXJZTZI/AAAAAAAAFs4/NpL7xT0ErYEj-MBGM6mq8RsYj-S_mawaQCLcB/s1600/Evans-Pi-1.png',
                                }} />
                            <Text style={styles.TextPri}>Môi Trường(12)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://cdn.pixabay.com/photo/2012/04/26/18/11/nucleus-42693_960_720.png',
                                }} />
                            <Text style={styles.TextPri}>Sức Khỏe(18)</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.ItemHome}>
                            <Image
                                style={styles.tinyLogo}
                                source={{
                                    uri: 'https://lh3.googleusercontent.com/proxy/FYlo5EFWH82p8lOrbJZ3B0HJZ2p18dHsikxX8bNVdpDyr4Fb_znfJSXAW1ZSRTeJaSjSAruBWH5QhaL6nsFF2AoaOGSaJaX5uM7eQzq8hz1i_UEofbK4Hl8iaimpgR1_3JKU7GcWRYWWRlrTMXFSCKCj9L7W7HoRXp_YzCfY_sylbqQyHlBlBhZAfb-RntS_BUmlBQ',
                                }} />
                            <Text style={styles.TextPri}>Văn Học(24)</Text>
                        </View>
                    </TouchableOpacity>

                </View>
                </ScrollView>
            </View>

        );
    }
}
const styles = StyleSheet.create({
    container: {
        height: 700,
        width: "100%",

    },
    tinyLogo: {
        width: 75,
        height: 75,
        marginTop: 2

    },
    flexRow: {
        justifyContent: 'space-between',
        flexDirection: 'row',

    },
    ItemHome: {
        flexDirection:'column',
        width: 110,
        height: 120,
        backgroundColor: 'white',
        borderRadius: 12,
        shadowColor: "rgba(0,0,0,1)",
        shadowOffset: {
            width: 3,
            height: 3
        },
        elevation: 10,
        shadowOpacity: 0.20,
        shadowRadius: 11,
        overflow: "scroll",
        marginTop:18,
        alignItems:'center',
      

    },
    TextPri: {
        fontSize:15,
        alignItems:'center',
    }

});